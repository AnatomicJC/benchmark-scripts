#!/bin/sh

# https://www.alibabacloud.com/blog/how-to-use-fio-to-test-the-io-performance-of-ecs-local-ssd-and-essd-part-1_597783
# https://linuxreviews.org/HOWTO_Test_Disk_I/O_Performance

# Run with this command (script execution time will be displayed at the end of the log):
# { time sh fio.sh > fio.log ; } 2>> fio.log

# At the end, filter with this command:
# grep -A1 -B1 -E "(Run status group|IOPS)" fio.log | less

# Random write IOPS (4 KB for single I/O):
fio 01-rand-write-testing.fio

echo
echo =========================
echo

# Random read IOPS (4KB for single I/O):
fio 02-rand-read-testing.fio

echo
echo =========================
echo

# Sequential write throughput (write bandwidth) (1024 KB for single I/O):
fio 03-write-pps-testing.fio

echo
echo =========================
echo

# Sequential read throughput (read bandwidth) (1024 KB for single I/O):
fio 04-read-pps-testing.fio

echo
echo =========================
echo

# Random write latency (4 KB for single I/O):
fio 05-rand-write-latency-testing.fio

echo
echo =========================
echo

# Random read latency (4KB for single I/O):
fio 06-rand-read-latency-testing.fio

echo
echo =========================
echo

rm iotest
