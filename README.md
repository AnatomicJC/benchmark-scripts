# Benchmark scripts

## pveperf

Benchmark tool from Proxmox. Important values are fsync per second and buffered read.

Example output:

```
# ./pveperf
CPU BOGOMIPS:      217194.24
REGEX/SECOND:      5569348
HD SIZE:           59.94 GB (/dev/md2)
BUFFERED READS:    2176.61 MB/sec
AVERAGE SEEK TIME: 0.02 ms
FSYNCS/SECOND:     13120.06
DNS EXT:           13.66 ms
```

On vanilla Debian / Ubuntu, you need to install these dependencies:

```
apt install libfile-sync-perl libnet-dns-resolver-programmable-perl
```

## fio

Resources:

* https://www.alibabacloud.com/blog/how-to-use-fio-to-test-the-io-performance-of-ecs-local-ssd-and-essd-part-1_597783
* https://linuxreviews.org/HOWTO_Test_Disk_I/O_Performance

You can run benchs with this command (script execution time will be displayed at the end of the log):

```
{ time sh fio.sh > fio.log ; } 2>> fio.log
```

Imortant values is IOPS, you can filter output log with this command:

```
grep -A1 -B1 -E "(Run status group|IOPS)" fio.log | less
```
