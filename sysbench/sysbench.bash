#!/usr/bin/env bash

# Taille totale des fichiers de test
FILE_TOTAL_SIZE=10G
# Durée de chaque test en secondes
TIME=300
# Obtenir le nombre de CPU
THREADS=$(nproc)

# Préparer le test
sysbench fileio --file-total-size=$FILE_TOTAL_SIZE prepare

# Liste des modes de test
modes=("seqwr" "seqrewr" "seqrd" "rndrd" "rndwr" "rndrw")

# Boucle pour tester chaque mode
for mode in "${modes[@]}"
do
  echo "Running test in $mode mode with $THREADS threads"
  sysbench fileio --file-total-size=$FILE_TOTAL_SIZE --file-test-mode=$mode --time=$TIME --max-requests=0 --threads=$THREADS run
done

# Nettoyer après les tests
sysbench fileio --file-total-size=$FILE_TOTAL_SIZE cleanup

echo "All tests completed"

