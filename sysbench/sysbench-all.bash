#!/usr/bin/env bash

# Obtenir le nombre de CPU
THREADS=$(nproc)

# Fonction pour exécuter les tests de CPU
function test_cpu {
  echo "Running CPU test"
  sysbench cpu --cpu-max-prime=20000 run
}

# Fonction pour exécuter les tests de mémoire
function test_memory {
  echo "Running Memory test"
  sysbench memory --memory-block-size=1M --memory-total-size=10G run
}

# Fonction pour exécuter les tests de threads
function test_threads {
  echo "Running Threads test"
  sysbench threads --threads=$THREADS --time=300 run
}

# Fonction pour exécuter les tests de mutex
function test_mutex {
  echo "Running Mutex test"
  sysbench mutex --mutex-num=4096 --threads=$THREADS --time=300 run
}

# Fonction pour exécuter les tests de performances disque
function test_disk {
  FILE_TOTAL_SIZE=10G
  TIME=300

  sysbench fileio --file-total-size=$FILE_TOTAL_SIZE prepare

  modes=("seqwr" "seqrewr" "seqrd" "rndrd" "rndwr" "rndrw")

  for mode in "${modes[@]}"
  do
    echo "Running Disk test in $mode mode with $THREADS threads"
    sysbench fileio --file-total-size=$FILE_TOTAL_SIZE --file-test-mode=$mode --time=$TIME --max-requests=0 --threads=$THREADS run
  done

  sysbench fileio --file-total-size=$FILE_TOTAL_SIZE cleanup
}

# Exécuter tous les tests
test_cpu
test_memory
test_threads
test_mutex
test_disk

echo "All tests completed"

